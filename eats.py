class Bear():
    def __init__(self, eats, first):
        self.eats = eats
        self.firstname = first
    def __eq__(self, other):
        return self.firstname() == other.firstname()
    def __ne__(self, other):
        return self.firstname() != other.firstname()
    def __lt__(self, other):
        return self.firstname() <= other.firstname()
    def __gt__(self, other):
        return self.firstname() <= other.firstname()

class Turtle():
    def __init__(self, eats, readlev):
        self.eats = eats
        if readlev == 'K':
            readlev = 0
            self.readlev = readlev
        else:
            self.readlev = readlev
    def __eq__(self, other):
        return self.readlev() == other.readlev()
    def __ne__(self, other):
        return self.readlev() != other.readlev()
    def __lt__(self, other):
        return self.readlev() <= other.readlev()
    def __gt__(self, other):
        return self.readlev() <= other.readlev()
        
class Sasquatch():
    def __init__(self, eats, ac):
        self.eats = eats
        self.ac = ac;
    def __eq__(self, other):
        return self.ac() == other.ac()
    def __ne__(self, other):
        return self.ac() == other.ac()

Yogi = Bear("berries", "Yogi")
BooBoo = Bear("berries", "Boo Boo")
Po = Bear("berries", "Master Po")
Smokey = Bear("berries", "Smokey the Bear")
Fozzie = Bear("berries", "Fozzie Bear")
print("Bear Evaluations:")
print(f"Fozzie is alphabetically before Po is a {Fozzie.firstname <= Po.firstname} statement!")
print(Smokey.firstname >= BooBoo.firstname)
print(BooBoo.firstname == Yogi.firstname)
print(Yogi.firstname == Yogi.firstname)
print("")

Gamera = Turtle("insects and small fish", "K")
Michaelangelo = Turtle("insects and small fish", 4)
Raphael = Turtle("insects and small fish", 4)
Donatello = Turtle("insects and small fish", 12)
Leonardo = Turtle("insects and small fish", 9)
print("Comparative Turtle Reading Levels:")
print("Gamera has a higher reading level than Donatello." if Gamera.readlev >= Donatello.readlev else "Donatello has a higher reading level than Gamera")
print("Michaelangelo has the same reading level as Raphael." if Michaelangelo.readlev == Raphael.readlev else "Michaelangelo and Raphael don't have the same reading level!")
print("")

Aloysius = Sasquatch("hazelnuts", "541")
Bubba = Sasquatch("hazelnuts", "360")
Lily = Sasquatch("hazelnuts", "509")
Sam = Sasquatch("hazelnuts", "509")
print("Sasquatch Adjacency by Area Code:")
print("Aloysius doesn't live near Bubba." if Aloysius.ac != Bubba.ac else "Aloysius and Bubba live in the same area code.")
print("Lily doesn't live near Sam." if Lily.ac != Sam.ac else "Lily and Sam live in the same area code.")

