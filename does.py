class Electronic():
    count = 0
    def __init__ (self, IO):
        self.IO = IO
        Electronic.count += 1
    def IOswitch(self, IO):
        if self.IO is "On":
            self.IO = "Off"
        else:
            self.IO = "On"

class HeadLight(Electronic):
    def __init__ (self, does, IO):
        super().__init__(IO)
        self.does = does
    def showfunc(self):
        return self.does + "!"

class DriveTrain:
    def __init__ (self, does):
        self.does = does
    def showfunc(self):
        return self.does + "!"

class SoundSystem(Electronic):
    def __init__ (self, does, IO):
        super().__init__(IO)
        self.does = does
    def showfunc(self):
        return self.does + "!"

class Car:
    def __init__ (self, HeadLight, DriveTrain, SoundSystem):
        self.HeadLight = None
        self.DriveTrain = None
        self.SoundSystem = None
    def does(self):
        self.HeadLight = HeadLight('illuminates', 'On')
        self.DriveTrain = DriveTrain('powers')
        self.SoundSystem = SoundSystem('rocks', 'Off')
        print(f"This car has a Drive Train that {self.DriveTrain.showfunc()}")
        print(f"It also has Head Lights that {self.HeadLight.showfunc()}")
        print(f"The Head Lights are {self.HeadLight.IO}")
        print(f"And it has a Sound System that {self.SoundSystem.showfunc()}")
        print(f"The Sound System are {self.SoundSystem.IO}")
        print(f"There are {Electronic.count} electronic components in this car.")

gremlin = Car(HeadLight, DriveTrain, SoundSystem)
gremlin.does()
